//
//  PatternGenerator.swift
//  WeaveMe
//
//  Created by  Kurt Schaefer on 6/5/18.
//  Copyright © 2018 RetroTechJurnal. All rights reserved.
//

import UIKit

class PatternGenerator: NSObject {
    init(image: UIImage) {
        guard let inputCGImage = image.cgImage else {
            NSLog("PatternGenerator: Unable to get cgImage")
            return
        }

        guard let dataProvider = inputCGImage.dataProvider else {
            NSLog("PatternGenerator: Unable to get data provider for image")
            return
        }

        if inputCGImage.bitsPerPixel != 32 {
            NSLog("WeavingTileManager: We currently only support RGBA images.)")
            return
        }

        let pixelData = dataProvider.data
        let data: UnsafePointer<UInt8> = CFDataGetBytePtr(pixelData)

        let pixelWidth = inputCGImage.width
        let pixelHeight = inputCGImage.height
        let bytesPerPixel = inputCGImage.bitsPerPixel/8

        var pixelIndex: Int = 0

        for weftIndex in 0...pixelHeight - 1 {
            var weftRow: [Bool] = []
            for warpIndex in 0...pixelWidth - 1 {
                pixelIndex = ((pixelWidth*weftIndex) + warpIndex) * bytesPerPixel
                // Right now we just look at the R channel and look above/below.   We should do something
                // smarter.
                weftRow.append(data[pixelIndex] > 128)
            }
            weftOnTop.append(weftRow)
        }
    }

}
