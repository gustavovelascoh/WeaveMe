//
//  PatternCropLayer.swift
//  WeaveMe
//
//  Created by  Kurt Schaefer on 8/18/18.
//  Copyright © 2018 RetroTechJurnal. All rights reserved.
//
//  This layer takes a primary layer, and a warp weft coordinates rect and crops the
//  layer to the rect.   It also can take an edge layer which can be added to the
//  cropped primary pattern at the top/bottom/left/right
//
//  So for example if we wanted to have 1 top/bottom plain weave rows
//  and 1 left/right pain weave column at the edges we could set
//  edgeLayer as a plain weave layer, and the final size would be the cropped
//  size + 2 in each dimension.
//
import UIKit

class PatternCropLayer: PatternLayer {
    // The pattern layer we are cropping/adding an edge to.
    var primaryLayer: PatternLayer? {
        didSet {
            updateUniqueColors()
        }
    }
    
    // Pattern to use at the edge.  If this is nil edgeTop/Left/Right/Bottom are ignored.
    var edgeLayer: PatternLayer? {
        didSet {
            updateUniqueColors()
        }
    }
    
    /// A rectangle in primaryLayer warp/weft coordinates that we will crop to.
    /// If nil the plain edges are added to the outside of the full extent of the pattern.
    var cropRect: CGRect?
    
    /// Number of rows/columns to add at the various edges to form the final pattern.
    /// These are added after the crop.  (If there is one.)
    var edgeTop = 1
    var edgeBottom = 1
    var edgeLeft = 1
    var edgeRight = 1
    
    // If you want edge thread properties like color and diameter to
    // come from the primary layer instead of from the secondary layer you can set this
    // to true.
    var extendPrimaryLayerThreadPropertiesToEdges = true {
        didSet {
            updateUniqueColors()
        }
    }
    
    // This computes the thread by clipping to the clipping rect (if there is one) and
    // adding in however many edge pattern rows/columns.
    override func threadCount(_ type: ThreadType) -> Int {
        guard let workingRect = workingRect() else {
            return 0
        }
    
        if type == .warp {
            if edgeLayer != nil {
                return Int(workingRect.size.width) + edgeLeft + edgeRight
            }
            return Int(workingRect.size.width)
        }
        
        if edgeLayer != nil {
            return Int(workingRect.size.height) + edgeTop + edgeBottom
        }
      
        return Int(workingRect.size.height)
    }
    
    private func updateUniqueColors() {
        var layers: [PatternLayer] = []
        
        if let layer = primaryLayer {
            layers.append(layer)
        }
        
        if !extendPrimaryLayerThreadPropertiesToEdges {
            if let layer = edgeLayer {
                layers.append(layer)
            }
        }
        
        self.uniqueColors = mergeUniqueColors(layers: layers)
    }
    
    override func weftColor(y: Int) -> UIColor {
        if let primaryCoordinates = transformToExtendedPrimary(.warp, x: 0, y: y),
            let primaryLayer = primaryLayer {
            return primaryLayer.weftColor(y: primaryCoordinates.y)
        }
        return .clear
    }
    
    override func warpColor(x: Int) -> UIColor {
        if let primaryCoordinates = transformToExtendedPrimary(.weft, x: x, y: 0),
            let primaryLayer = primaryLayer {
            return primaryLayer.warpColor(x: primaryCoordinates.x)
        }
        return .clear
    }
    
    // This returns the full rect of the primaryLayer, cropped to the
    // cropping rect if there is one.
    private func workingRect() -> CGRect? {
        guard let primary = primaryLayer else {
            return nil
        }
        let layerRect = CGRect.init(x: 0, y: 0, width: primary.threadCount(.warp), height: primary.threadCount(.weft))
        
        if let cropRect = cropRect {
            return layerRect.intersection(cropRect)
        }
        return layerRect
    }
    
    // Returns nil if the coordinate is not part of the primaryLayer
    private func transformToPrimary(x: Int, y: Int) -> (x: Int, y: Int)? {
        guard let workingRect = workingRect() else {
                return nil
        }
        
        if edgeLayer == nil {
            return (x: x + Int(workingRect.origin.x), y: y + Int(workingRect.origin.y))
        }
        return (x: x - edgeLeft + Int(workingRect.origin.x), y: y - edgeTop + Int(workingRect.origin.y))
    }
    
    // Returns nil if the coordinate is not part of the edgeLayer.
    private func transformToEdge(x: Int, y: Int) -> (x: Int, y: Int)? {
        guard let workingRect = workingRect(),
            primaryLayer != nil,
            edgeLayer != nil else {
                return nil
        }
        let weftCount = threadCount(.weft)
        let warpCount = threadCount(.warp)
        
        if x < edgeLeft {
            return (x: x, y: y)
        }
        if x > (warpCount - 1) - edgeRight {
            return (x: x - Int(workingRect.size.width), y: y)
        }
        if y < edgeTop {
            return (x: x, y: y)
        }
        if y > (weftCount - 1) - edgeBottom {
            return (x: x, y: y - Int(workingRect.size.height))
        }
        return nil
    }
    
    // This takes a coordinate and tries to clamp it into the primary layer if it's in the edge.
    // This is used to support extendPrimaryLayerThreadPropertiesToEdges
    private func transformToExtendedPrimary(_ threadType: ThreadType, x: Int, y: Int) -> (x: Int, y: Int)? {
        let weftCount = threadCount(.weft)
        let warpCount = threadCount(.warp)
        
        var newX = x
        var newY = y
        
        if newX < edgeLeft {
            newX = edgeLeft + 1
        }
        if newX > (warpCount - 1) - edgeRight {
            newX = (warpCount - 1) - edgeRight
        }
        if newY < edgeTop {
            newY = edgeTop
        }
        if newY > (weftCount - 1) - edgeBottom {
            newY = (weftCount - 1) - edgeBottom
        }

        return transformToPrimary(x: newX, y: newY)
    }
    
    override func isOnTop(_ threadType: ThreadType, x: Int, y: Int) -> Bool {
        if let edgeCoordinate = transformToEdge(x: x, y: y),
            let edgeLayer = edgeLayer {
            // Don't add any weft isOnTop wefts to an overlay at the edge.
            if layerType == .overlay {
                return threadType == .warp
            }
            return edgeLayer.isOnTop(threadType, x: edgeCoordinate.x, y: edgeCoordinate.y)
        }
        if let primaryCoordinates = transformToPrimary(x: x, y: y),
            let primaryLayer = primaryLayer {
            return primaryLayer.isOnTop(threadType, x: primaryCoordinates.x, y: primaryCoordinates.y)
        }
        
        return false
    }
    
    override func setIsOnTop(_ threadType: ThreadType, x: Int, y: Int, value: Bool) {
        assert(false, "PatternCropLayers can not have their isOnTop set")
    }
    
    override func threadColor(_ threadType: ThreadType, x: Int, y: Int) -> UIColor {
        if let edgeCoordinate = transformToEdge(x: x, y: y),
            let edgeLayer = edgeLayer {
            if layerType == .overlay {
                return .clear
            } else if extendPrimaryLayerThreadPropertiesToEdges {
                if let primaryCoordinate = transformToExtendedPrimary(threadType, x: x, y: y),
                    let primaryLayer = primaryLayer {
                    return primaryLayer.threadColor(threadType, x: primaryCoordinate.x, y: primaryCoordinate.y)
                }
            }
            return edgeLayer.threadColor(threadType, x: edgeCoordinate.x, y: edgeCoordinate.y)
        }
        if let primaryCoordinate = transformToPrimary(x: x, y: y),
            let primaryLayer = primaryLayer {
            return primaryLayer.threadColor(threadType, x: primaryCoordinate.x, y: primaryCoordinate.y)
        }

        return .clear
    }
    
    override func threadDiameter(_ threadType: ThreadType, x: Int, y: Int) -> CGFloat {
        if let edgeCoordinate = transformToEdge(x: x, y: y),
            let edgeLayer = edgeLayer {
            if extendPrimaryLayerThreadPropertiesToEdges {
                if let primaryCoordinate = transformToExtendedPrimary(threadType, x: x, y: y),
                    let primaryLayer = primaryLayer {
                    return primaryLayer.threadDiameter(threadType, x: primaryCoordinate.x, y: primaryCoordinate.y)
                }
            }
            return edgeLayer.threadDiameter(threadType, x: edgeCoordinate.x, y: edgeCoordinate.y)
        }
        if let primaryCoordinates = transformToPrimary(x: x, y: y),
            let primaryLayer = primaryLayer {
            return primaryLayer.threadDiameter(threadType, x: primaryCoordinates.x, y: primaryCoordinates.y)
        }
        return 1.0
    }
    
    override func buildFromImage(_ image: UIImage, completion: (Error?) -> Void) {
        assert(false, "PatternCropLayers can not be set from an image.")
    }

    private class func maxDimensions(patternLayers: [PatternLayer]) -> (maxWidth: Int, maxHeight: Int) {
        var maxWidth = 0
        var maxHeight = 0

        for layer in patternLayers {
            let curWidth = layer.threadCount(.warp)
            if curWidth > maxWidth {
                maxWidth = curWidth
            }
            let curHeight = layer.threadCount(.weft)
            if curHeight > maxHeight {
                maxHeight = curHeight
            }
        }
        return (maxWidth: maxWidth, maxHeight: maxHeight)
    }

    // This wraps a plain edge around any pattern.  Right now we keep the blue and white default colors of
    // the plain weave pattern so it's visible that these are coming from someplace else, but really at least the
    // central warp colors should be coming from the primary. If leftRightInset is greater than zero the pattern is
    // eaten away by that many warp threads on the left and right.
    class func plainWeaveEdgedPatternFrom(_ pattern: Pattern, leftRightInset: Int, completion: @escaping (Pattern?, Error?) -> Void) {
        
        var anyOfTheLayersAreMixingLayers = false
        for layer in pattern.patternLayers where layer as? PatternMixingLayer != nil {
            anyOfTheLayersAreMixingLayers = true
            break
        }
        
        let plainLayer = PatternLayer()
        
        guard let plainImage = UIImage.init(named: anyOfTheLayersAreMixingLayers ? "Rib_1-2_shifted" : "Plain") else {
            completion(nil, PatternBuildError.imageCouldNotBeLoaded)
            return
        }
        
        let maxDimensions = self.maxDimensions(patternLayers: pattern.patternLayers)
        
        // If this mixing is being done to provide a tabby, it will be a smaller diameter
        plainLayer.defaultWeftThreadDiameter = 1.0
        plainLayer.buildFromImage(plainImage) { error in
            if let error = error {
                completion(nil, error)
                return
            }
            
            var anyOfTheLayersAreMixingLayers = false
            for layer in pattern.patternLayers where layer as? PatternMixingLayer != nil {
                anyOfTheLayersAreMixingLayers = true
                break
            }
            
            var cropLayers: [PatternCropLayer] = []
            for layer in pattern.patternLayers {
                let cropLayer = PatternCropLayer()

                // If we're going over a mixing layer we don't include the
                // bottom edge because that's already going to be a plain weave.
                if anyOfTheLayersAreMixingLayers {
                    cropLayer.edgeBottom = 0
                }
                cropLayer.edgeLeft = 4
                cropLayer.edgeRight = 4

                if leftRightInset > 0 {
                    cropLayer.cropRect = CGRect(x: leftRightInset, y: 0, width: layer.threadCount(.warp) - 2*leftRightInset,
                                                height: layer.threadCount(.weft))
                }
                
                cropLayer.primaryLayer = layer
                cropLayer.edgeLayer = plainLayer
                cropLayer.layerType =  layer.layerType
                
                // Make sure the edge pattern repeat enough to handle the total size of our pattern including additional edge rows/columns
                plainLayer.warpRepeats = maxDimensions.maxWidth
                plainLayer.weftRepeats = maxDimensions.maxHeight
                
                cropLayers.append(cropLayer)
            }
            let plainEdgedPattern = Pattern()
            plainEdgedPattern.patternLayers = cropLayers
            plainEdgedPattern.name = pattern.name
            
            completion(plainEdgedPattern, nil)
        }
    }
}
