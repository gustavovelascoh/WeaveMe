//
//  Pattern.swift
//  WeaveMe
//
//  Created by  Kurt Schaefer on 8/8/18.
//  Copyright © 2018 RetroTechJurnal. All rights reserved.
//
//  A weaving Pattern is build from a set of images.  It is simplest to make all the images the same size,
//  however using repeats you can for example have (imageWidth - 1)*warpRepeat be the same across all the images.
//
//  The first (0th) layer is the ground weave, and the rest of the layers are overlay layers (brocade shuttle style)
//  displayed above that with layers.last on top.
//
//  The top row and left column of pixels in the image define the default warp and weft colors for the pattern.
//  In the overlay layers the alpha should be 0 non pattern pixels for
//  the parts touched by the overlay and 0 in all the others  Those layers should only be one color since they define the ares touched by a specific brocade shuttle.
//
//  NOTE:
//  Eventually we may want to have a top level warp/weft repeat so that we can know the difference
//  between going on forever and reaching the edge of the loom, but right now my loom is so small this
//  isn't likely to matter for me.

import UIKit

class Pattern {
    var name = ""
    var thumbnailImage: UIImage?
    var uuid = ""

    // For now the thread count is just based on the first layer.
    // TODO:  Have loom information/repeats setup so we can get the actual thread
    // count.
    func threadCount(_ threadType: ThreadType) -> Int? {
        guard let layer = patternLayers.first else {
            return nil
        }
        return layer.threadCount(threadType)
    }
    
    var patternLayers: [PatternLayer] = []
    private var layerImages: [UIImage] = []
    
    // Set of unique colors that were in the images used to build the patter layers.
    // This is the union of all the unique colors in the PatternLayers.
    func uniqueColors() -> [UIColor] {
        if let firstLayer = patternLayers.first,
            patternLayers.count == 1 {
            return firstLayer.uniqueColors
        }

        // With more than one set of layers we have to merge.
        var hexStringToColor: [String: UIColor] = [:]
        
        for layer in patternLayers {
            for color in layer.uniqueColors {
                hexStringToColor[color.hexString()] = color
            }
        }
        
        // We pull out the colors from the dictionary and sort them.
        var uniqueColors = hexStringToColor.map { $0.value }
        uniqueColors.sort(by: UIColor.Comparison.hueAndValue)

        return uniqueColors
    }

    // Returns a weft pick for the yth row.
    func weftPicksForY(_ y: Int) -> [WeftPick] {
        var weftPicks: [WeftPick] = []
        for layer in patternLayers {
            weftPicks.append(contentsOf: layer.weftPicksForY(y))
        }
        return weftPicks
    }
    
    // This flushes out everything that is generated during a build.
    private func removeAllLayers() {
        patternLayers = []
        layerImages = []
    }
    
    // Loads the images, figures out the warp/weft colors and fills out the layer images.
    // This returns an error if it fails, or nil on success.
    private var buildFromImageNamesError: Error?
    func buildFromImageNames(_ imageNames: [String], completion: @escaping (Error?) -> Void) {
        removeAllLayers()
        
        // Use a dispatch group so we can build them all at the same time.
        let dispatchGroup = DispatchGroup()
        assert(buildFromImageNamesError == nil, "Pattern does not support nested build from image names")
        
        var patternLayerToIndex: [PatternLayer: Int] = [:]
        var layerIndex = 0
        for imageName in imageNames {

            // For now we assume that the first image is the base image and all others are overlay images.
            // If the loom supported multiple warps/other types of layers that may change
            let layerType: PatternLayer.LayerType = (layerIndex == 0) ? .ground : .overlay
            
            dispatchGroup.enter()
            let currentLayerIndex = layerIndex
            buildLayerFromImageName(imageName, layerType: layerType) { patternLayer, error in
                // We only hold on to the most recent error to flag that the build has failed.
                if error != nil {
                    self.buildFromImageNamesError = error
                } else if let patternLayer = patternLayer {
                    self.patternLayers.append(patternLayer)
                    patternLayerToIndex[patternLayer] = currentLayerIndex
                }
                dispatchGroup.leave()
            }
            layerIndex += 1
        }
        
        dispatchGroup.notify(queue: .main) {
            // If something went wrong flush any partially built stuff and return an error.
            if let error = self.buildFromImageNamesError {
                self.removeAllLayers()
                completion(error)
                return
            }
            
            // Maker sure they're in the same order as the imageNames that produced them, before we say we're done.
            self.patternLayers.sort(by: { patternLayerToIndex[$0] ?? 0 < patternLayerToIndex[$1] ?? 0 })
            completion(nil)
        }
    }
    
    private func buildLayerFromImageName(_ imageName: String, layerType: PatternLayer.LayerType, completion: @escaping (PatternLayer?, Error?) -> Void) {
        guard let image = UIImage.init(named: imageName) else {
            NSLog("Pattern: Unable to load image \(imageName)")
            completion(nil, PatternBuildError.imageCouldNotBeLoaded)
            return
        }
        buildLayerFromImage(image, layerType: layerType, completion: completion)
    }
    
    private func buildLayerFromImage(_ image: UIImage, layerType: PatternLayer.LayerType, completion: @escaping (PatternLayer?, Error?) -> Void) {
        let patternLayer = PatternLayer()
        
        patternLayer.layerType = layerType
        patternLayer.buildFromImage(image) { error in
            if error != nil {
                completion(nil, error)
            } else {
                completion(patternLayer, nil)
            }
        }
    }
}
