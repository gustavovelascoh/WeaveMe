//
//  PatternLayerView.swift
//  WeaveMe
//
//  Created by  Kurt Schaefer on 8/12/18.
//  Copyright © 2018 RetroTechJurnal. All rights reserved.
//
//  A basic view for displaying some or all of a PatternLayer.  
//

import UIKit

class PatternLayerView: UIView {

    let patternLayer: PatternLayer
    let patternRenderSettings: RenderSettings
    
    /// Create using this, then call render and then make it visible however you please.
    init(frame: CGRect, patternLayer: PatternLayer, renderSettings: RenderSettings) {
        self.patternLayer = patternLayer
        self.patternRenderSettings = renderSettings
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    /// Call this to have the view render the images it needs to display at the target width. If this is successful
    /// the view is ready to animate in.
    func render(renderer: PatternRenderer, completion: @escaping (_ success: Bool, Error?) -> Void) {
        renderer.renderPatternLayerRect(patternLayer, style: patternRenderSettings.style,
                                        rect: patternRenderSettings.rect, targetOutputWidth:
        patternRenderSettings.outputImageWidth) { image, error in
            guard let image = image else {
                completion(false, error)
                return
            }
            self.setupImageView()
            guard let imageView = self.imageView else {
                completion(false, error)
                return
            }
            imageView.image = image
            completion(true, nil)
        }
    }
    
    private var imageView: UIImageView?
    private func setupImageView() {
        let imageView = UIImageView()
        self.imageView = imageView
        imageView.contentMode = .scaleAspectFit
        imageView.frame = bounds
        imageView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        addSubview(imageView)
    }

}
