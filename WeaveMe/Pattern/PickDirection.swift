//
//  PickDirection.swift
//  WeaveMe
//
//  Created by  Kurt Schaefer on 9/15/18.
//  Copyright © 2018 RetroTechJurnal. All rights reserved.
//

import Foundation

enum PickDirection: Int, CustomStringConvertible {
    case leftToRight = 0
    case rightToLeft = 1
    
    var description: String {
        if self == .leftToRight {
            return "leftToRight"
        }
        return "rightToLeft"
    }
}
