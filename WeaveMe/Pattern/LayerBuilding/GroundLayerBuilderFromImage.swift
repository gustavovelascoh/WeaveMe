//
//  GroundLayerBuilderFromImage.swift
//  WeaveMe
//
//  Created by  Kurt Schaefer on 9/30/18.
//  Copyright © 2018 RetroTechJurnal. All rights reserved.
//
//  This uses an image to generate the weftOnTop, warpColors and weftColors for
//  a ground layer.  It returns a build item with the generated LayerBuildItem.

import UIKit

class GroundLayerBuilderFromImage {

    /// Takes an image and generates the buildItem for it including weftOnTop, warpColors, and weftColors.
    func buildFromImage(_ image: UIImage, completion: (_ buildItem: LayerBuildItem?, _ error: Error?) -> Void) {
        guard let inputCGImage = image.cgImage else {
            NSLog("Pattern: Unable to get cgImage")
            completion(nil, PatternBuildError.imageCouldNotBeLoaded)
            return
        }

        guard let dataProvider = inputCGImage.dataProvider else {
            NSLog("Pattern: Unable to get data provider for image")
            completion(nil, PatternBuildError.imageCouldNotBeLoaded)
            return
        }

        if inputCGImage.bitsPerPixel != 32 {
            NSLog("Pattern: We currently only support RGBA images.)")
            completion(nil, PatternBuildError.imageIsNotRGBA)
            return
        }

        let pixelData = dataProvider.data
        let data: UnsafePointer<UInt8> = CFDataGetBytePtr(pixelData)

        let pixelWidth = inputCGImage.width
        let pixelHeight = inputCGImage.height
        let bytesPerPixel = inputCGImage.bitsPerPixel/8

        var pixelIndex: Int = 0

        if pixelWidth < 2 || pixelHeight < 2 {
            NSLog("Pattern: The image is only \(pixelWidth) x \(pixelHeight).  It must be at least 2 x 2.")
            completion(nil, PatternBuildError.imageIsTooSmall)
            return
        }

        var buildItem = LayerBuildItem()

        // First read out all the colors.
        buildItem.warpColors = buildWarpColors(pixelWidth: pixelWidth, bytesPerPixel: bytesPerPixel, data: data)
        buildItem.weftColors = buildWeftColors(pixelWidth: pixelWidth, pixelHeight: pixelHeight, bytesPerPixel: bytesPerPixel, data: data)

        // Right now we just use red channel midpoint to define which is higher.
        // In theory these should probably be black and white, but it's kind of nice to
        // have them be something closer to the native colors for preview.
        for weftIndex in 1...pixelHeight - 1 {
            var weftRow: [Bool] = []
            let weftColorPixelIndex = ((pixelWidth*weftIndex) + 0) * bytesPerPixel
            let weftR = Int(data[weftColorPixelIndex])
            let weftG = Int(data[weftColorPixelIndex + 1])
            let weftB = Int(data[weftColorPixelIndex + 2])

            for warpIndex in 1...pixelWidth - 1 {
                pixelIndex = ((pixelWidth*weftIndex) + warpIndex) * bytesPerPixel

                let sumOfColorDiff = (abs(Int(data[pixelIndex]) - weftR) +
                    abs(Int(data[pixelIndex + 1]) - weftG) +
                    abs(Int(data[pixelIndex + 2]) - weftB))
                // If the color is very close to the declared weft color we declare it weftOnTop.
                // We may want to do something smarter if the warp color is very close to the weft color or something.
                weftRow.append(sumOfColorDiff < 10)
                // weftRow.append(data[pixelIndex] > 128)
            }
            buildItem.weftOnTop.append(weftRow)
        }

        completion(buildItem, nil)
    }

    // Read the top row of pixels from left to right and return an array of colors.
    private func buildWarpColors(pixelWidth: Int, bytesPerPixel: Int, data: UnsafePointer<UInt8>) -> [UIColor] {
        var warpColors: [UIColor] = []

        var pixelIndex = 0
        for warpIndex in 1..<pixelWidth {
            pixelIndex = warpIndex*bytesPerPixel
            let color = UIColor.init(red: CGFloat(data[pixelIndex])/255.0,
                                     green: CGFloat(data[pixelIndex + 1])/255.0,
                                     blue: CGFloat(data[pixelIndex + 2])/255.0,
                                     alpha: CGFloat(data[pixelIndex + 3])/255.0)
            warpColors.append(color)
        }

        return warpColors
    }

    // Read the left most column of pixels from top to bottom and return an array of them.

    // By explicitly listing the weft colors in the first column we can support
    // ground weaves with a patterned full row, which is distinct from patterns made up
    // with more layers since those are driven by a brocade shuttle.
    private func buildWeftColors(pixelWidth: Int, pixelHeight: Int, bytesPerPixel: Int, data: UnsafePointer<UInt8>) -> [UIColor] {
        var weftColors: [UIColor] = []

        var pixelIndex = 0
        for weftIndex in 1..<pixelHeight {
            pixelIndex = pixelWidth*weftIndex*bytesPerPixel
            let color = UIColor.init(red: CGFloat(data[pixelIndex])/255.0,
                                     green: CGFloat(data[pixelIndex + 1])/255.0,
                                     blue: CGFloat(data[pixelIndex + 2])/255.0,
                                     alpha: CGFloat(data[pixelIndex + 3])/255.0)
            weftColors.append(color)
        }

        return weftColors
    }

}
