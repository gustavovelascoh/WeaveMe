//
//  PatternLayer.swift
//  WeaveMe
//
//  Created by  Kurt Schaefer on 8/8/18.
//  Copyright © 2018 RetroTechJurnal. All rights reserved.
//
//  This helper class keeps track of the information that defines a single layer of the pattern.
//  Mostly this is a 2D grid of weftOnTop bools + some color information.  This forms the core
//  pattern which can have a 1 or more repeats.  We also store some other things like
//  unique colors, etc.   We may eventually want to store other information here.
//
//  Right now there are two basic layer types.  ground, and overlay.  A Pattern is made up of
//  an ordered set of these PatternLayers.

import UIKit

class PatternLayer: NSObject {
    
    // Ground layers have a full set of warp/weft colors where overlay layers
    // only have weft colors.  However we still represent the interface for the colors as a uniform set
    // of warp/weft colors even for the overlay, but all the weftOnTop = true colors will be the weft color and all the
    // all the weftOnTop = false colors are alpha 0.

    // TODO: Ground double is a wacky one were we assume 2 colors one the weft color and one the
    // warp color presuming there is only one non weft warp color.  this really is just a test and
    // we should probably remove it if this doesn't turn out to be a useful thing.
    
    enum LayerType {
        case ground
        case overlay
    }
    
    // In general internally we correspond warp to x and weft y.
    
    var weftOnTop: [[Bool]] = []  // Warp by weft array of "is weft on top" bools.
    
    // Small repeating patterns can repeat multiple times to make up a larger pattern.
    var warpRepeats: Int = 1
    var weftRepeats: Int = 1
    
    // These are built from the 0th row/column of the image.  If we are a ground layer.
    // Although color lookups can be larger if there are repeats the array size should
    // match the coreThreadCount.
    var coreWarpColors: [UIColor] = []
    var coreWeftColors: [UIColor] = []
    
    static var defaultWarpColor: UIColor = .black
    static var defaultWeftColor: UIColor = .white

    // All the colors that appear in the warp/weft.
    var uniqueColors: [UIColor] = []
    
    func warpColor(x: Int) -> UIColor {
        if coreWarpColors.isEmpty {
            return PatternLayer.defaultWarpColor
        }
        return coreWarpColors[x%coreWarpColors.count]
    }
    
    func weftColor(y: Int) -> UIColor {
        if coreWeftColors.isEmpty {
            return PatternLayer.defaultWeftColor
        }
        return coreWeftColors[y%coreWeftColors.count]
    }
    
    /// Helper method to take an array of colors and return the unique ones
    /// sorted by hueAndValue.
    func getUniqueColors(_ colors: [UIColor]) -> [UIColor] {
        // With more than one set of layers we have to merge.
        var hexStringToColor: [String: UIColor] = [:]
        
        for color in colors {
            hexStringToColor[color.hexString()] = color
        }
    
        // We pull out the colors from the dictionary and sort them.
        var newUniqueColors = hexStringToColor.map { $0.value }
        newUniqueColors.sort(by: UIColor.Comparison.hueAndValue)
        
        return newUniqueColors
    }
    
    // This is a helper function that some subclasses may like to use in order
    // to figure out all the unique colors across multiple layers.
    func mergeUniqueColors(layers: [PatternLayer]) -> [UIColor] {
        if let firstLayer = layers.first,
            layers.count == 1 {
            return firstLayer.uniqueColors
        }
        
        // With more than one set of layers we have to merge.
        var colors: [UIColor] = []
        
        for layer in layers {
            colors.append(contentsOf: layer.uniqueColors)
        }

        return getUniqueColors(colors)
    }
    
    var layerType: LayerType = .ground
    
    private func onTopRangeForThread(_ threadType: ThreadType, y: Int) -> (minX: Int, maxX: Int)? {
        var min: Int?
        var max: Int?
        let warpCount = threadCount(.warp)
        for x in 0..<warpCount {
            if isOnTop(threadType, x: x, y: y) {
                if min == nil {
                    min = x
                }
                max = x
            }
        }
        guard let minX = min,
            let maxX = max else {
                return nil
        }
        return (minX: minX, maxX: maxX)
    }
    
    private func overlayWeftPicksForY(_ y: Int) -> [WeftPick] {
        let color = weftColor(y: y)
        if color == .clear {
            return []
        }
        guard let minMax = onTopRangeForThread(.weft, y: y) else {
            return []
        }
        
        let weftPick = WeftPick.init(patternLayer: self, threadColor: color, threadDiameter: threadDiameter(.weft, x: 0, y: y),
                                    direction: y%2 == 1 ? PickDirection.leftToRight : PickDirection.rightToLeft ,
                                    leverType: .positive, y: y, minX: minMax.minX, maxX: minMax.maxX)
        return [weftPick]
    }
    
    // Returns a weft pick for the row where "Row" is a 1 -> weft count index from bottom to top.
    // This idea of row matches what the user sees.
    func weftPicksForY(_ y: Int) -> [WeftPick] {
        if weftPicksByY.isEmpty {
            buildAllWeftPicks()
        }
        return weftPicksByY[y]
    }
    
    func weftPicksForYInternal(_ y: Int) -> [WeftPick] {
        var weftPicks: [WeftPick] = []
        let warpCount = threadCount(.warp)
        
        // Ground and double ground layers always go all the way across.
        if layerType == .ground {
            let weftPick = WeftPick.init(patternLayer: self, threadColor: weftColor(y: y), threadDiameter: threadDiameter(.weft, x: 0, y: y),
                                         direction: y%2 == 1 ? PickDirection.leftToRight : PickDirection.rightToLeft ,
                                         leverType: .positive, y: y, minX: 0, maxX: warpCount - 1)
            weftPicks.append(weftPick)
        } else if layerType == .overlay {
            weftPicks.append(contentsOf: overlayWeftPicksForY(y))
        }
        
        return weftPicks
    }
    
    private var weftPicksByY: [[WeftPick]] = []
    private func buildAllWeftPicks() {
        let weftCount = threadCount(.weft)
        weftPicksByY.removeAll()
        weftPicksByY.reserveCapacity(weftCount)
        
        for y in 0..<weftCount {
            weftPicksByY.append(weftPicksForYInternal(y))
        }
        
        updatePickDirections(weftPicks: weftPicksByY)
    }
    
    // Because things like mixing layers can have different kinds of picks on different
    // rows we compute the direction based on alternating per pick type, not per y row.
    private func updatePickDirections(weftPicks: [[WeftPick]]) {
        var threadSignatureToCount: [String: Int] = [:]
        
        for y in (0...(weftPicks.count - 1)).reversed() {
            let picks = weftPicks[y]
            for pick in picks {
                let pickCount = threadSignatureToCount[pick.threadSignature] ?? 0
                // Ground and overlay layers are inverted just to help keep shuttle on opposite sides.
                if layerType == .ground {
                    pick.direction = pickCount%2 == 1 ? PickDirection.leftToRight : PickDirection.rightToLeft
                } else {
                    pick.direction = pickCount%2 == 1 ? PickDirection.rightToLeft : PickDirection.leftToRight
                }
                threadSignatureToCount[pick.threadSignature] = pickCount + 1
            }
        }
    }

    // Get the warp/weft dimensions of the pattern layer.  This will be (image.width - 1)*warpRepeat x (image.height - 1)*warpRepeat
    // because image row/column 0 is reserved for warp colors
    func threadCount(_ threadType: ThreadType) -> Int {
        if threadType == .weft {
            return coreThreadCount(.weft)*weftRepeats
        }
        return coreThreadCount(.warp)*warpRepeats
    }
    
    // The core pattern is the size of the weftOnTop array.  It can be smaller
    // than the threadCount when the pattern repeats.
    private func coreThreadCount(_ threadType: ThreadType) -> Int {
        if threadType == .weft {
        return weftOnTop.count
        }
        if weftOnTop.count == 0 {
        return 0
        }
        return weftOnTop[0].count
    }
    
    func isOnTop(_ threadType: ThreadType, x: Int, y: Int) -> Bool {
        var x = x
        var y = y
        
        // First clip to the max range, and then mod to perform the repeat.
        if y < 0 {
            y = 0
        } else if y > weftOnTop.count*weftRepeats - 1 {
            y = weftOnTop.count*weftRepeats - 1
        }
        y = y % weftOnTop.count
        
        if x < 0 {
            x = 0
        } else if x > weftOnTop[y].count*warpRepeats - 1 {
            x = weftOnTop[y].count*warpRepeats - 1
        }
        x  = x % weftOnTop[y].count
        
        if threadType == .weft {
            return weftOnTop[y][x]
        }
        
        return !weftOnTop[y][x]
    }
    
    func setIsOnTop(_ threadType: ThreadType, x: Int, y: Int, value: Bool) {
        let coreWarpCount = coreThreadCount(.warp)
        let coreWeftCount = coreThreadCount(.weft)
        
        // If there is no pattern you can't set it.
        if coreWarpCount == 0 || coreWeftCount == 0 {
            return
        }
        let x = x%coreWarpCount
        let y = y%coreWeftCount
        if threadType == .weft {
            weftOnTop[y][x] = value
        } else {
            weftOnTop[y][x] = !value
        }
    }
    
    func threadColor(_ threadType: ThreadType, x: Int, y: Int) -> UIColor {
        // If it's a ground layer they can ask about warp and weft colors independently.
        if layerType == .ground {
            if threadType == .weft {
                return weftColor(y: y)
            } else {
                return warpColor(x: x)
            }
        } else {
            // If it's an overlay layer, they can only ask about weft color, otherwise everything is .clear
            if threadType == .weft && isOnTop(.weft, x: x, y: y) {
                return weftColor(y: y)
            }
        }
        return .clear
    }
    
    var defaultWarpThreadDiameter: CGFloat = 1.0
    var defaultWeftThreadDiameter: CGFloat = 1.0
    
    /// In the base class this just returns the default warp/weft diameters but
    /// fancier subclasses can do more with this if needed.
    func threadDiameter(_ threadType: ThreadType, x: Int, y: Int) -> CGFloat {
        if threadType == .warp {
            return defaultWarpThreadDiameter
        }
        return defaultWeftThreadDiameter
    }
    
    func buildFromImage(_ image: UIImage, completion: (Error?) -> Void) {
        switch layerType {
        case .ground:
            buildFromGroundImage(image, completion: completion)
        case .overlay:
            buildFromOverlayImage(image, completion: completion)
        }
    }

    private func removeAllBuiltInformation() {
        weftOnTop = []
        coreWarpColors = []
        coreWeftColors = []
        uniqueColors = []
    }

    private func buildFromOverlayImage(_ image: UIImage, completion: (Error?) -> Void) {
        let layerBuilder = OverlayLayerBuilderFromImage()

        layerBuilder.buildFromImage(image) { buildItem, error in

            guard let buildItem = buildItem else {
                guard let error = error else {
                    completion(PatternBuildError.builderFailedWithNoError)
                    return
                }
                completion(error)
                return
            }

            self.removeAllBuiltInformation()

            self.weftOnTop = buildItem.weftOnTop
            self.coreWeftColors = buildItem.weftColors

            let visibleWeftColors = buildItem.weftColors.filter { $0 != .clear }
            self.uniqueColors = self.getUniqueColors(visibleWeftColors)
            self.uniqueColors.sort(by: UIColor.Comparison.hueAndValue)

            self.layerType = .overlay

            completion(nil)
        }
    }
    
    // This builds warpColor, weftColor, and weftOnTop info from the given image.
    private func buildFromGroundImage(_ image: UIImage, completion: (Error?) -> Void) {
        let layerBuilder = GroundLayerBuilderFromImage()

        layerBuilder.buildFromImage(image) { buildItem, error in

            guard let buildItem = buildItem else {
                guard let error = error else {
                    completion(PatternBuildError.builderFailedWithNoError)
                    return
                }
                completion(error)
                return
            }

            self.removeAllBuiltInformation()

            self.weftOnTop = buildItem.weftOnTop
            self.coreWarpColors = buildItem.warpColors
            self.coreWeftColors = buildItem.weftColors

            var allColors: [UIColor] = []
            allColors.append(contentsOf: buildItem.warpColors)
            allColors.append(contentsOf: buildItem.weftColors)

            self.uniqueColors = self.getUniqueColors(allColors)
            self.uniqueColors.sort(by: UIColor.Comparison.hueAndValue)

            self.layerType = .ground

            completion(nil)
        }
    }
}
