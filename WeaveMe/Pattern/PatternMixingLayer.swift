//
//  PatternMixingLayer.swift
//  WeaveMe
//
//  Created by  Kurt Schaefer on 8/12/18.
//  Copyright © 2018 RetroTechJurnal. All rights reserved.
//
//  For some weaving effects we want to control some structural pattern (like a plain weave)
//  inside another pattern.  For example if we want every other row to be an incrementing plain weave
//  you can mix together the two patterns.  And you can author one without having to do something ugly
//  like draw plain weave in on every other row.
//
//  The warp color/diameter comes from the primary layer.

import UIKit

class PatternMixingLayer: PatternLayer {
    private(set) var primaryLayer: PatternLayer?
    private(set) var secondaryLayer: PatternLayer?
    
    // Right now we only provide weft mixing in this fairly simple way.  So in the default
    // y = 0 would produce the pattern from the primaryLayer for primaryLayerConsecutiveWefts rows,
    // and then you'd get secondaryLayerConsecutiveWefts rows from the secondaryLayer, etc.
    var primaryLayerConsecutiveWefts: Int = 1
    var secondaryLayerConsecutiveWefts: Int = 1
    
    func setLayers(primary: PatternLayer, secondary: PatternLayer) {
        primaryLayer = primary
        secondaryLayer = secondary
        cloneWarp(from: primaryLayer, to: secondaryLayer)
        updateUniqueColors()
    }
    
    // This computes the thread count driven by the size of the primary
    // plus whatever amount of secondary is going to be inserted.
    override func threadCount(_ type: ThreadType) -> Int {
        guard let primary = primaryLayer,
            let secondary = secondaryLayer else {
                return 0
        }
        
        if type == .warp {
            return primary.threadCount(type)
        }
        
        let primaryWeftCount = primary.threadCount(.weft)
        let secondaryWeftCount = secondary.threadCount(.weft)
        
        if primaryWeftCount == 0 {
            return 0
        }
        
        if secondaryWeftCount == 0 {
            return primaryWeftCount
        }
    
        let numberOfSecondaryInjections = Int(floor(Double(primaryWeftCount)/Double(primaryLayerConsecutiveWefts)))
        return primaryWeftCount + numberOfSecondaryInjections*secondaryLayerConsecutiveWefts
    }
    
    // The warps all come from the primary pattern, so we clone those over to avoid
    // getting extra colors/weirdness from the secondary pattern.
    private func cloneWarp(from primary: PatternLayer?, to secondary: PatternLayer?) {
        guard let primary = primary,
            let secondary = secondary else {
                return
        }
        
        secondary.coreWarpColors = primary.coreWarpColors
        secondary.defaultWarpThreadDiameter = primary.defaultWarpThreadDiameter
        secondary.uniqueColors = secondary.getUniqueColors(secondary.coreWarpColors)
        // TODO: If we eventually support per-thread diameters we should clone those here.
    }
    
    private func updateUniqueColors() {
        var layers: [PatternLayer] = []
        
        if let layer = primaryLayer {
            layers.append(layer)
        }
        
        if let layer = secondaryLayer {
            layers.append(layer)
        }
        
        self.uniqueColors = mergeUniqueColors(layers: layers)
    }
    
    private func getPatternLayerAndYValue(for y: Int) -> (patternLayer: PatternLayer, y: Int) {
        guard let primaryLayer = primaryLayer,
            let secondaryLayer = secondaryLayer else {
            assert(false, "Primary and secondary layers must be set before they can be probed.")
            return (patternLayer: PatternLayer(), y: 0)
        }

        let primaryWeftCount = primaryLayer.threadCount(.weft)
        let secondaryWeftCount = secondaryLayer.threadCount(.weft)

        let strideLength = primaryLayerConsecutiveWefts + secondaryLayerConsecutiveWefts
        let strideIndex = y%strideLength
        let strideCount = Int(floor(Double(y)/Double(strideLength)))
        
        // This row is in the primary pattern
        if strideIndex < primaryLayerConsecutiveWefts {
            let totalPrimaryThreadsBelowUs = y - strideCount*secondaryLayerConsecutiveWefts
            return (patternLayer: primaryLayer, y: totalPrimaryThreadsBelowUs%primaryWeftCount)
        }
        
        // We use + 1 because even this incomplete stride has a primary below us.
        let totalSecondaryThreadsBelowUs = y - (strideCount + 1)*primaryLayerConsecutiveWefts
        return (patternLayer: secondaryLayer, y: totalSecondaryThreadsBelowUs%secondaryWeftCount)
    }
    
    override func isOnTop(_ threadType: ThreadType, x: Int, y: Int) -> Bool {
        let layerAndY = getPatternLayerAndYValue(for: y)
        if layerType == .overlay && layerAndY.patternLayer != primaryLayer {
            return false
        }
        return layerAndY.patternLayer.isOnTop(threadType, x: x, y: layerAndY.y)
    }
    
    override func threadDiameter(_ threadType: ThreadType, x: Int, y: Int) -> CGFloat {
        let layerAndY = getPatternLayerAndYValue(for: y)
        return layerAndY.patternLayer.threadDiameter(threadType, x: x, y: layerAndY.y)
    }
    
    override func setIsOnTop(_ threadType: ThreadType, x: Int, y: Int, value: Bool) {
        assert(false, "PatternMixingLayers can not have their isOnTop set")
    }
    
    override func warpColor(x: Int) -> UIColor {
        guard let primaryLayer = primaryLayer else {
            return .clear
        }
        return primaryLayer.warpColor(x: x)
    }
    
    override func weftColor(y: Int) -> UIColor {
        let layerAndY = getPatternLayerAndYValue(for: y)
        if layerType == .overlay && layerAndY.patternLayer != primaryLayer {
            return .clear
        }
        return layerAndY.patternLayer.weftColor(y: layerAndY.y)
    }
    
    override func threadColor(_ threadType: ThreadType, x: Int, y: Int) -> UIColor {
        let layerAndY = getPatternLayerAndYValue(for: y)
        return layerAndY.patternLayer.threadColor(threadType, x: x, y: layerAndY.y)
    }
    
    override func buildFromImage(_ image: UIImage, completion: (Error?) -> Void) {
        assert(false, "PatternMixingLayers can not be set from an image.")
    }
    
    /// Build a bunch of layers based on this image.
    private class func buildLayersFromImage(image: UIImage, count: Int, completion: @escaping ([PatternLayer]?, Error?) -> Void) {
        let dispatchGroup = DispatchGroup()
        var errors: [Error] = []
        var layers: [PatternLayer] = []
        
        for _ in 0..<count {
            let layer = PatternLayer()
            dispatchGroup.enter()
            layer.buildFromImage(image) { error in
                if let error = error {
                    errors.append(error)
                } else {
                    layers.append(layer)
                }
                dispatchGroup.leave()
            }
        }
        
        dispatchGroup.notify(queue: .main) {
            if let firstError = errors.first {
                completion(nil, firstError)
            } else {
                completion(layers, nil)
            }
        }
    }
    
    class func mixPlainWeaveFromPattern(_ pattern: Pattern, plainWeaveWeftColor: UIColor, completion: @escaping (Pattern?, Error?) -> Void) {
        guard let plainImage = UIImage.init(named: "Plain") else {
            completion(nil, PatternBuildError.imageCouldNotBeLoaded)
            return
        }
        
        var maxWidth = 0
        var maxHeight = 0
        
        for layer in pattern.patternLayers {
            let curWidth = layer.threadCount(.warp)
            if curWidth > maxWidth {
                maxWidth = curWidth
            }
            let curHeight = layer.threadCount(.weft)
            if curHeight > maxHeight {
                maxHeight = curHeight
            }
        }
        
        // If this mixing is being done to provide a tabby, it will be a smaller diameter
        // So we create a plainLayer for every layer in the pattern so there's no danger of
        // them stomping on each other for things like warp cloning.
        buildLayersFromImage(image: plainImage, count: pattern.patternLayers.count) { plainLayers, error in
            if let error = error {
                completion(nil, error)
                return
            }
            
            var mixLayers: [PatternMixingLayer] = []
            var plainLayerIndex = 0
            for layer in pattern.patternLayers {
                let mixLayer = PatternMixingLayer()
                guard let plainLayer = plainLayers?[plainLayerIndex] else {
                    assertionFailure()
                    continue
                }
                plainLayerIndex += 1
                
                // We use a slightly creamy color injection
                plainLayer.defaultWeftThreadDiameter = 0.5
                plainLayer.coreWeftColors = [plainWeaveWeftColor]
                
                mixLayer.setLayers(primary: layer, secondary: plainLayer)
                mixLayer.layerType =  layer.layerType
                
                // Make sure the edge pattern repeat enough to handle the total size of our pattern including additional edge rows/columns
                plainLayer.warpRepeats = maxWidth
                plainLayer.weftRepeats = maxHeight
                
                mixLayers.append(mixLayer)
            }
            let mixedPattern = Pattern()
            mixedPattern.patternLayers = mixLayers
            mixedPattern.name = pattern.name
            
            completion(mixedPattern, nil)
        }
    }
}
