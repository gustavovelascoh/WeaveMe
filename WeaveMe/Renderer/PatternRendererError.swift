//
//  PatternRendererError.swift
//  WeaveMe
//
//  Created by  Kurt Schaefer on 8/10/18.
//  Copyright © 2018 RetroTechJurnal. All rights reserved.
//

import Foundation

enum PatternRenderError: Int, Error, LocalizedError, CustomStringConvertible {
    
    case couldNotGetGraphicsContext = 170
    case renderRectDoesNotFitInPattern = 171
    case unableToMergeRenderedLayers = 172
    
    var errorDescription: String? {
        switch self {
        case .couldNotGetGraphicsContext:
            return "The renderer was unable to get the graphics a graphics context."
        case .renderRectDoesNotFitInPattern:
            return "The requested render rectangle does not fit in the given pattern layer."
        case .unableToMergeRenderedLayers:
            return "The render resulted in no layer images so there was nothing to merge."
        }
    }
    
    var description: String {
        return "\(errorDescription ?? "Unknown") (\(type(of: self)).\(self.rawValue))"
    }
    
}
