//
//  PatternRenderer.swift
//  WeaveMe
//
//  Created by  Kurt Schaefer on 8/10/18.
//  Copyright © 2018 RetroTechJurnal. All rights reserved.
//
//  This object is used to render images of patterns.  This can include
//  both individual PatternLayer images, and full Pattern thumbnails.
//
//  TODO: I'm sure this falls down horribly when thread image tiles end up being < 1 thread per pixel.
//  So we may want to deal with that case at some point in the future, but because of current use
//  pattern it's not worth supporting.
//

import UIKit

class PatternRenderer: NSObject {
    
    // We may eventually want to support other rendering styles
    enum RenderStyle {
        case normalWarpAndWeft  // The most standard, both warp and weft drawing mixed in depth order
        case normalWarp         // Warp Only
        case normalWeft         // Weft Only
        case normalOnTopWarp    // Warp sections that are above the weft, but nothing else.
        case normalOnTopWeft    // Weft sections that are above the warp, but nothing else.
    }
    
    private func warpImageTypeForTile(patternLayer: PatternLayer, x: Int, y: Int) -> ThreadImageType {
        // Warp shape is dependent on y neighbors and is all the vertical images.
        if patternLayer.isOnTop(.warp, x: x, y: y-1) {
            if patternLayer.isOnTop(.warp, x: x, y: y) {
                if patternLayer.isOnTop(.warp, x: x, y: y+1) {
                    return .uuuVert
                } else {
                    return .uudVert
                }
            } else {
                if patternLayer.isOnTop(.warp, x: x, y: y+1) {
                    return .dddVert
                } else {
                    return .dddVert
                }
            }
        } else {
            if patternLayer.isOnTop(.warp, x: x, y: y) {
                if patternLayer.isOnTop(.warp, x: x, y: y+1) {
                    return .duuVert
                } else {
                    return .dudVert
                }
            } else {
                if patternLayer.isOnTop(.warp, x: x, y: y+1) {
                    return .dddVert
                } else {
                    return .dddVert
                }
            }
        }
    }

    private func weftImageTypeForTile(patternLayer: PatternLayer, x: Int, y: Int) -> ThreadImageType {
        // Weft shape is dependent on x neighbors and is all the horizontal images.
        if patternLayer.isOnTop(.weft, x: x-1, y: y) {
            if patternLayer.isOnTop(.weft, x: x, y: y) {
                if patternLayer.isOnTop(.weft, x: x+1, y: y) {
                    return .uuuHoriz
                } else {
                    return .uudHoriz
                }
            } else {
                if patternLayer.isOnTop(.weft, x: x+1, y: y) {
                    return .dddHoriz
                } else {
                    return .dddHoriz
                }
            }
        } else {
            if patternLayer.isOnTop(.weft, x: x, y: y) {
                if patternLayer.isOnTop(.weft, x: x+1, y: y) {
                    return .duuHoriz
                } else {
                    return .dudHoriz
                }
            } else {
                if patternLayer.isOnTop(.weft, x: x+1, y: y) {
                    return .dddHoriz
                } else {
                    return .dddHoriz
                }
            }
        }
    }

    private func imageTypeForTile(patternLayer: PatternLayer, x: Int, y: Int, type: ThreadType) -> ThreadImageType {
        switch type {
        case .warp:
            return warpImageTypeForTile(patternLayer: patternLayer, x: x, y: y)
        case .weft:
            return weftImageTypeForTile(patternLayer: patternLayer, x: x, y: y)
        }
    }

    private func tintedImageForTile(imageSource: WeavingImageSource, patternLayer: PatternLayer,
                                    x: Int, y: Int, tintColor: UIColor, type: ThreadType) -> UIImage? {
        let imageType = imageTypeForTile(patternLayer: patternLayer, x: x, y: y, type: type)
        return imageSource.tintedImageForType(imageType, tintColor: tintColor)
    }

    private func scaleRectByThreadDiameter(threadType: ThreadType, warpDiameter: CGFloat, weftDiameter: CGFloat, rect: CGRect) -> CGRect {
        if threadType == .warp {
            if warpDiameter == 1.0 {
                return rect
            } else {
                let newWidth = rect.width*warpDiameter
                let offset = (rect.size.width - newWidth)/2.0
                return CGRect.init(x: rect.origin.x + offset, y: rect.origin.y, width: newWidth, height: rect.height)
            }
        }
        if weftDiameter == 1.0 {
            return rect
        } else {
            let newHeight = rect.size.height*weftDiameter
            let offset = (rect.height - newHeight)/2.0
            return CGRect.init(x: rect.origin.x, y: rect.origin.y + offset, width: rect.width, height: newHeight)
        }
    }
    
    /// This renders a rectangle of the given pattern into an images suitable for filling a targetOutputWidth view.
    /// The size may vary slightly to provide uniform alignment of the tiles used to make the image.
    /// The rect is in pattern thread coordinates between 0 and thread count in both directions.
    /// Right now we only support some minimum rendering styles.
    func renderPatternLayerRect(_ patternLayer: PatternLayer, style: RenderStyle,
                                rect: CGRect, targetOutputWidth: CGFloat,
                                completion: @escaping (UIImage?, Error?) -> Void) {
        
        // We compute the tile height/width in the given output size
        // which is just targetOutputWidth/rect.size.width  This is different
        // than the number of pixels in each tile.

        let xRange: ClosedRange<Int> = Int(rect.minX)...Int(rect.maxX)
        let yRange: ClosedRange<Int> = Int(rect.minY)...Int(rect.maxY)
        
        let tileWidth = ceil(targetOutputWidth/rect.size.width)   // Pixel align the drawing so the tiles don't overlap at all.
        let tileHeight = tileWidth
        let imageRect = CGRect(x: 0, y: 0, width: tileWidth*rect.size.width, height: tileHeight*rect.size.height)

        DispatchQueue.global().async {
            // Build an image context the target size, with alpha channel, and the native device scale.
            let imageSize = CGSize.init(width: imageRect.width, height: imageRect.height)
            UIGraphicsBeginImageContextWithOptions(imageSize, false, 0)
            
            guard let context = UIGraphicsGetCurrentContext() else {
                UIGraphicsEndImageContext()
                completion(nil, PatternRenderError.couldNotGetGraphicsContext)
                return
            }
            
            // Clear out the image.
            UIColor.clear.setFill()
            context.fill(imageRect)

            // Instead of doing clipping we just punt.
            if rect.minX < 0 || rect.minY < 0 ||
                xRange.upperBound > patternLayer.threadCount(.warp) ||
                yRange.upperBound > patternLayer.threadCount(.weft) {
                completion(nil, PatternRenderError.renderRectDoesNotFitInPattern)
                return
            }

            let imageSource = WeavingImageSource()
            imageSource.nativeImageSize = CGSize(width: tileWidth, height: tileHeight)

            switch style {
            case .normalWarpAndWeft:
                self.drawNormalWarpAndWeft(in: context, patternLayer: patternLayer, imageSource: imageSource, xRange: xRange, yRange: yRange)
            case .normalWeft:
                self.drawNormalWeft(in: context, patternLayer: patternLayer, imageSource: imageSource, xRange: xRange, yRange: yRange)
            case .normalWarp:
                self.drawNormalWarp(in: context, patternLayer: patternLayer, imageSource: imageSource, xRange: xRange, yRange: yRange)
            case .normalOnTopWarp:
                self.drawNormalOnTopWarp(in: context, patternLayer: patternLayer, imageSource: imageSource, xRange: xRange, yRange: yRange)
            case .normalOnTopWeft:
                self.drawNormalOnTopWeft(in: context, patternLayer: patternLayer, imageSource: imageSource, xRange: xRange, yRange: yRange)
            }
            
            let newImage = UIGraphicsGetImageFromCurrentImageContext()
            UIGraphicsEndImageContext()
            
            DispatchQueue.main.async {
                completion(newImage, nil)
            }
        }
    }

    private func drawNormalWarpAndWeft(in context: CGContext, patternLayer: PatternLayer,
                                       imageSource: WeavingImageSource, xRange: ClosedRange<Int>, yRange: ClosedRange<Int>) {
        for y in yRange {
            if patternLayer.layerType == .overlay && patternLayer.weftColor(y: y) == .clear {
                continue
            }
            for x in xRange {
                let tileRect = getNativeTileRect(x: x, y: y, xRange: xRange, yRange: yRange, imageSource: imageSource)
                let warpDiameter = patternLayer.threadDiameter(.warp, x: x, y: y)
                let weftDiameter = patternLayer.threadDiameter(.weft, x: x, y: y)

                let weftOnTop = patternLayer.isOnTop(.weft, x: x, y: y)
                let bottomThreadType: ThreadType = weftOnTop ? .warp : .weft
                let topThreadType: ThreadType = weftOnTop ? .weft : .warp

                let bottomColor = patternLayer.threadColor(bottomThreadType, x: x, y: y)
                let topColor = patternLayer.threadColor(topThreadType, x: x, y: y)

                let bottomImage = tintedImageForTile(imageSource: imageSource, patternLayer: patternLayer,
                                                     x: x, y: y, tintColor: bottomColor, type: bottomThreadType)
                let topImage = tintedImageForTile(imageSource: imageSource, patternLayer: patternLayer,
                                                  x: x, y: y, tintColor: topColor, type: topThreadType)

                bottomImage?.draw(in: scaleRectByThreadDiameter(threadType: bottomThreadType, warpDiameter: warpDiameter, weftDiameter: weftDiameter, rect: tileRect))
                topImage?.draw(in: scaleRectByThreadDiameter(threadType: topThreadType, warpDiameter: warpDiameter, weftDiameter: weftDiameter, rect: tileRect))
            }
        }
    }

    private func drawNormalWeft(in context: CGContext, patternLayer: PatternLayer,
                                imageSource: WeavingImageSource, xRange: ClosedRange<Int>, yRange: ClosedRange<Int>) {
        for y in yRange {
            if patternLayer.layerType == .overlay && patternLayer.weftColor(y: y) == .clear {
                continue
            }
            for x in xRange {
                let tileRect = getNativeTileRect(x: x, y: y, xRange: xRange, yRange: yRange, imageSource: imageSource)
                let warpDiameter = patternLayer.threadDiameter(.warp, x: x, y: y)
                let weftDiameter = patternLayer.threadDiameter(.weft, x: x, y: y)
                let color = patternLayer.threadColor(.weft, x: x, y: y)
                let image = tintedImageForTile(imageSource: imageSource, patternLayer: patternLayer, x: x, y: y, tintColor: color, type: .weft)

                image?.draw(in: scaleRectByThreadDiameter(threadType: .weft, warpDiameter: warpDiameter, weftDiameter: weftDiameter, rect: tileRect))
            }
        }
    }

    private func drawNormalWarp(in context: CGContext, patternLayer: PatternLayer,
                                imageSource: WeavingImageSource, xRange: ClosedRange<Int>, yRange: ClosedRange<Int>) {
        // Overlays never contribute a warp because the warp comes from some layer below the overlay.
        if patternLayer.layerType == .overlay {
            return
        }
        for y in yRange {
            for x in xRange {
                let tileRect = getNativeTileRect(x: x, y: y, xRange: xRange, yRange: yRange, imageSource: imageSource)
                let warpDiameter = patternLayer.threadDiameter(.warp, x: x, y: y)
                let weftDiameter = patternLayer.threadDiameter(.weft, x: x, y: y)
                let color = patternLayer.threadColor(.warp, x: x, y: y)
                let image = tintedImageForTile(imageSource: imageSource, patternLayer: patternLayer, x: x, y: y, tintColor: color, type: .warp)

                image?.draw(in: scaleRectByThreadDiameter(threadType: .warp, warpDiameter: warpDiameter, weftDiameter: weftDiameter, rect: tileRect))
            }
        }
    }

    private func drawNormalOnTopWarp(in context: CGContext, patternLayer: PatternLayer,
                                     imageSource: WeavingImageSource, xRange: ClosedRange<Int>, yRange: ClosedRange<Int>) {
        // Overlays never contribute a warp because the warp comes from some layer below the overlay.
        if patternLayer.layerType == .overlay {
            return
        }
        for y in yRange {
            for x in xRange {
                if patternLayer.isOnTop(.warp, x: x, y: y) {
                    let tileRect = getNativeTileRect(x: x, y: y, xRange: xRange, yRange: yRange, imageSource: imageSource)
                    let warpDiameter = patternLayer.threadDiameter(.warp, x: x, y: y)
                    let weftDiameter = patternLayer.threadDiameter(.weft, x: x, y: y)

                    let color = patternLayer.threadColor(.warp, x: x, y: y)
                    let image = self.tintedImageForTile(imageSource: imageSource,
                                                        patternLayer: patternLayer, x: x, y: y, tintColor: color, type: .warp)
                    image?.draw(in: self.scaleRectByThreadDiameter(threadType: .warp, warpDiameter: warpDiameter, weftDiameter: weftDiameter, rect: tileRect))
                }
            }
        }
    }

    private func drawNormalOnTopWeft(in context: CGContext, patternLayer: PatternLayer,
                                     imageSource: WeavingImageSource, xRange: ClosedRange<Int>, yRange: ClosedRange<Int>) {
        for y in yRange {
            if patternLayer.layerType == .overlay && patternLayer.weftColor(y: y) == .clear {
                continue
            }
            for x in xRange {
                if patternLayer.isOnTop(.weft, x: x, y: y) {
                    let tileRect = getNativeTileRect(x: x, y: y, xRange: xRange, yRange: yRange, imageSource: imageSource)
                    let warpDiameter = patternLayer.threadDiameter(.warp, x: x, y: y)
                    let weftDiameter = patternLayer.threadDiameter(.weft, x: x, y: y)

                    let color = patternLayer.threadColor(.weft, x: x, y: y)
                    let image = self.tintedImageForTile(imageSource: imageSource,
                                                        patternLayer: patternLayer, x: x, y: y, tintColor: color, type: .weft)
                    image?.draw(in: self.scaleRectByThreadDiameter(threadType: .weft, warpDiameter: warpDiameter, weftDiameter: weftDiameter, rect: tileRect))
                }
            }
        }
    }

    private func getNativeTileRect(x: Int, y: Int, xRange: ClosedRange<Int>, yRange: ClosedRange<Int>, imageSource: WeavingImageSource) -> CGRect {
        let tileX = CGFloat(x - xRange.lowerBound)*imageSource.nativeImageSize.width
        let tileY = CGFloat(y - yRange.lowerBound)*imageSource.nativeImageSize.height
        return CGRect.init(x: tileX, y: tileY, width: imageSource.nativeImageSize.width, height: imageSource.nativeImageSize.height)
    }

    // This renders each of the layers in order and then composites them down into a single image.
    func render(pattern: Pattern, style: RenderStyle,
                rect: CGRect, targetOutputWidth: CGFloat,
                completion: @escaping (UIImage?, Error?) -> Void) {
        var layerToImage: [PatternLayer: UIImage] = [:]
        var errors: [Error] = []

        let dispatchGroup = DispatchGroup()
        for layer in pattern.patternLayers {
            dispatchGroup.enter()
            renderPatternLayerRect(layer, style: style, rect: rect, targetOutputWidth: targetOutputWidth) { (image, error) in
                if let layerImage = image {
                    layerToImage[layer] = layerImage
                } else if let error = error {
                    errors.append(error)
                }
                dispatchGroup.leave()
            }
        }

        dispatchGroup.notify(queue: .main) {
            // If something went wrong we notify about the first error.
            if let firstError = errors.first {
                completion(nil, firstError)
                return
            }

            var orderedImages: [UIImage] = []
            for layer in pattern.patternLayers {
                if let image = layerToImage[layer] {
                    orderedImages.append(image)
                } else {
                    assertionFailure("Mysteriously missing image for layer.")
                }
            }

            // If we got all the images we merge them together into a single image.
            self.mergeImages(orderedImages, completion: completion)
        }
    }

    // This takes a set of images and draws them all into a single image.
    // The output size is based on the size of the first image.
    private func mergeImages(_ images: [UIImage], completion: @escaping (UIImage?, Error?) -> Void) {
        guard let firstImage = images.first else {
            completion(nil, PatternRenderError.unableToMergeRenderedLayers)
            return
        }

        // If there's only one image we don't have to do any work.
        if images.count == 1 {
            completion(firstImage, nil)
        }

        DispatchQueue.global().async {
            // Build an image context the target size, with alpha channel, and the native device scale.
            let imageWidth = firstImage.size.width
            let imageHeight = firstImage.size.height
            let imageSize = CGSize.init(width: imageWidth, height: imageHeight)
            UIGraphicsBeginImageContextWithOptions(imageSize, false, 0)

            guard let context = UIGraphicsGetCurrentContext() else {
                UIGraphicsEndImageContext()
                completion(nil, PatternRenderError.couldNotGetGraphicsContext)
                return
            }

            let fullImageRect = CGRect(x: 0.0, y: 0.0, width: imageWidth, height: imageHeight)

            // Clear out the image.
            UIColor.clear.setFill()
            context.fill(fullImageRect)

            for image in images {
                image.draw(in: fullImageRect)
            }

            let newImage = UIGraphicsGetImageFromCurrentImageContext()
            UIGraphicsEndImageContext()

            DispatchQueue.main.async {
                completion(newImage, nil)
            }
        }
    }
}
