//
//  PatternBuilder.swift
//  WeaveMe
//
//  Created by Kurt Schaefer on 6/15/19.
//  Copyright © 2019 RetroTechJurnal. All rights reserved.
//
//  This helper takes a patternDescription and turns it into a pattern.  This is how the

import UIKit

class PatternBuilder {

    func buildPattern(from patternDescription: PatternDescription, completion: @escaping (_ pattern: Pattern?, _ error: Error?) -> Void) {
        let pattern = Pattern()

        pattern.buildFromImageNames(patternDescription.imageNames) { error in
            if let error = error {
                completion(nil, error)
                return
            }

            // We wrap the finalization of the pattern so it can be applied only to the top level pattern and the end.
            let callCompletion: (Pattern?, Error?) -> Void = { pattern, error in
                pattern?.uuid = patternDescription.uuid
                pattern?.name = patternDescription.name
                completion(pattern, error)
            }

            if let warpRepeates = patternDescription.warpRepeats {
                pattern.patternLayers.first?.warpRepeats = warpRepeates
            }
            if let weftRepeates = patternDescription.weftRepeats {
                pattern.patternLayers.first?.weftRepeats = weftRepeates
            }
            if let defaultWarpThreadDiameter = patternDescription.defaultWarpThreadDiameter {
                pattern.patternLayers.first?.defaultWarpThreadDiameter = defaultWarpThreadDiameter
            }

            // If both tabby and edging used we always put the edging outside of the tabby.
            if let tabbyWeftColor = patternDescription.tabbyWeftColor {
                PatternMixingLayer.mixPlainWeaveFromPattern(pattern, plainWeaveWeftColor: tabbyWeftColor) { mixedPattern, error in
                    if let mixedPattern = mixedPattern {
                        if let edgeInset = patternDescription.plainWeaveLeftRightEdgeInset {
                            PatternCropLayer.plainWeaveEdgedPatternFrom(mixedPattern, leftRightInset: edgeInset) { edgedPattern, error in
                                callCompletion(edgedPattern, error)
                            }
                        } else {
                            callCompletion(mixedPattern, nil)
                        }
                    } else {
                        callCompletion(nil, error)
                    }
                }
                return
            }

            if let edgeInset = patternDescription.plainWeaveLeftRightEdgeInset {
                PatternCropLayer.plainWeaveEdgedPatternFrom(pattern, leftRightInset: edgeInset) { edgedPattern, error in
                   callCompletion(edgedPattern, error)
                }
                return
            }

            callCompletion(pattern, nil)
        }
    }

}
