//
//  PatternScrollViewControllerDelegate.swift
//  WeaveMe
//
//  Created by  Kurt Schaefer on 9/9/18.
//  Copyright © 2018 RetroTechJurnal. All rights reserved.
//

import UIKit

protocol PatternScrollViewControllerDelegate: class {
    
    /// Returns the location of the tap in pattern x/y space.
    /// You can turn this tap off with tapIsEnabled false on the controller.
    func patternScrollViewController(_ scrollViewController: PatternScrollViewController, didTapOnPatternPoint point: CGPoint)
    
}
