//
//  PatternScrollViewController.swift
//  WeaveMe
//
//  Created by  Kurt Schaefer on 9/8/18.
//  Copyright © 2018 RetroTechJurnal. All rights reserved.
//
//  This contains the smarts about displaying/highlighting a pattern in a scroll view.
//

import UIKit

class PatternScrollViewController: UIViewController {

    weak var delegate: PatternScrollViewControllerDelegate?
    
    var pattern: Pattern? {
        didSet {
            if isViewLoaded {
                patternScrollView.pattern = pattern
            }
        }
    }
    
    /// Should tapping on the scroll view result in taps that are sent to the delegate.
    var tapIsEnabled: Bool {
        set {
            patternTapGesture.isEnabled = newValue
        }
        get {
            return patternTapGesture.isEnabled
        }
    }
    
    @IBOutlet var patternScrollView: PatternScrollView!
    @IBOutlet var patternTapGesture: UITapGestureRecognizer!
    
    private var viewBounds: CGRect?
    private var renderer = PatternRenderer()
    
    /// Compute the view space fame of the threads involved with the pick.
    private func frameForWeftPick(_ weftPick: WeftPick) -> CGRect? {
        guard let bounds = viewBounds,
        let warpCount = pattern?.threadCount(.warp) else {
            return nil
        }
        if warpCount == 0 {
            return nil
        }
        
        let scale = bounds.size.width / CGFloat(warpCount)
        return CGRect.init(x: CGFloat(weftPick.minX)*scale, y: CGFloat(weftPick.y)*scale,
                                     width: CGFloat((weftPick.maxX - weftPick.minX) + 1)*scale,
                                     height: 1.0*scale)
    }
    
    // We might want to do something like brightness shifting instead, but for now we
    // just use black/white.
    private func highlightColor(for weftColor: UIColor) -> UIColor {
        let hsv = weftColor.hsv
        if hsv.value < 0.5 {
            return .white
        }
        return .black
    }
    
    private var highlightPickViews: [WeftPickView] = []
    func highlightPick(_ weftPick: WeftPick, animated: Bool, completion: @escaping ((Bool) -> Void)) {
        guard let pattern = pattern,
            let pickFrame = frameForWeftPick(weftPick),
            let groundLayer = pattern.patternLayers.first else {
                return
        }
        let highlightView = WeftPickView.init(frame: pickFrame)
        
        // This is not the best way to keep this on screen.  It sucks to have to move out from under
        // your finger if you're tapping around.  So lame.
        if let scrollView = view as? UIScrollView {
            scrollView.scrollRectToVisible(pickFrame.insetBy(dx: 0.0, dy: -100.0), animated: animated)
        }
        
        highlightView.renderPick(weftPick, warpSourceLayer: groundLayer, renderer: renderer) { success, error in
            if !success {
                NSLog("Failed to render highlight \(error?.localizedDescription ?? "nil")")
                completion(false)
                return
            }
            highlightView.frame = pickFrame
            highlightView.alpha = 0.0
            self.highlightPickViews.append(highlightView)
            self.patternScrollView.addSubview(highlightView)
            
            highlightView.backgroundColor = self.highlightColor(for: weftPick.threadColor)
            highlightView.transform = CGAffineTransform(scaleX: 1.0, y: 0.001)
            UIView.animate(withDuration: 0.4, delay: 0.0,
                           usingSpringWithDamping: 0.5, initialSpringVelocity: 0.0, options: [], animations: {
                            highlightView.transform = .identity
                            highlightView.alpha = 1.0
            }, completion: completion)
        }
    }
    
    func unhighlightAll(animated: Bool, completion: @escaping ((Bool) -> Void)) {
        UIView.animate(withDuration: animated ? 0.1 : 0.0, animations: {
            for layerView in self.highlightPickViews {
                layerView.alpha = 0.0
            }
        }, completion: { (finished) in
            for layerView in self.highlightPickViews {
                layerView.removeFromSuperview()
            }
            self.highlightPickViews.removeAll()
            completion(finished)
        })
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        patternScrollView.pattern = pattern
    }

    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        viewBounds = view.bounds
    }
    
    @IBAction func patternTap(_ sender: UITapGestureRecognizer) {
        if sender.state != .ended {
            return
        }
        guard let warpCount = pattern?.threadCount(.warp),
            let weftCount = pattern?.threadCount(.weft) else {
                return
        }
        let location = sender.location(in: view)
        let warpStep = patternScrollView.contentSize.width / CGFloat(warpCount)
        let weftStep = patternScrollView.contentSize.height / CGFloat(weftCount)
        let patternPosition = CGPoint.init(x: Int(location.x/warpStep),
                                           y: Int(location.y/weftStep))
        
        delegate?.patternScrollViewController(self, didTapOnPatternPoint: patternPosition)
    }
    
}
