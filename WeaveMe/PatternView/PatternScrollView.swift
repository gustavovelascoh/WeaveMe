//
//  PatternScrollView.swift
//  WeaveMe
//
//  Created by  Kurt Schaefer on 8/23/18.
//  Copyright © 2018 RetroTechJurnal. All rights reserved.
//
//  This is a scrolling view for exploring a pattern.  Right now it only
//  supports a few internal image views that are the full size of the pattern, but
//  for big complex patterns this may eventually handle things like incrementally
//  rendering sections, allowing zooming, etc.
//
//  This also has the ability to highlight various parts of a pattern to help users
//  step though the various picks needed to weave the pattern.
//

import UIKit

class PatternScrollView: UIScrollView {

    var pattern: Pattern?
    
    private func renderAllPatternLayers(targetOutputWidth: CGFloat, completion: @escaping ([PatternLayer: UIImageView]?, Error?) -> Void) {
        guard let pattern = pattern else {
            completion(nil, nil)
            return
        }
        
        var errors: [Error] = []
        var layerToImage: [PatternLayer: UIImage] = [:]
        let dispatchGroup = DispatchGroup()
        
        for layer in pattern.patternLayers {
            // Right now we're using a different renderer for each layer.  Not sure if that's best, however we should eventually
            // do something where the renderer pre-a-prized of the size, so it can natively make some tile images that
            // don't have to be scaled way down.  That should speed rendering up.
            let renderer = PatternRenderer()
            guard let width = pattern.threadCount(.warp),
                let height = pattern.threadCount(.weft) else {
                    continue
            }
            dispatchGroup.enter()
            let style: PatternRenderer.RenderStyle = layer.layerType == .overlay ? .normalOnTopWeft : .normalWarpAndWeft
            renderer.renderPatternLayerRect(layer, style: style, rect: CGRect.init(x: 0, y: 0, width: width, height: height), targetOutputWidth: targetOutputWidth) { image, error in
                if let image = image {
                    layerToImage[layer] = image
                } else {
                    if let error = error {
                        errors.append(error)
                    }
                }
                dispatchGroup.leave()
            }
        }
        
        dispatchGroup.notify(queue: .main) {

            // If any of the layer renders failed we just report an error.
            if let anError = errors.first {
                completion(nil, anError)
                return
            }
            
            var layerToImageView: [PatternLayer: UIImageView] = [:]
            for layer in pattern.patternLayers {
                if let image = layerToImage[layer] {
                    let imageView = UIImageView.init(image: image)
                    imageView.contentMode = .scaleAspectFit
                    
                    var outputHeight: CGFloat = 0.0
                    if image.size.width != 0 && image.size.height != 0 {
                        let aspect: CGFloat = CGFloat(image.size.height)/CGFloat(image.size.width)
                        outputHeight = targetOutputWidth*aspect
                    }
                    
                    imageView.frame = CGRect.init(x: 0.0, y: 0.0, width: targetOutputWidth, height: outputHeight)
                    
                    layerToImageView[layer] = imageView
                }
            }
            completion(layerToImageView, nil)
        }
    }

    private var patternLayerToImageView: [PatternLayer: UIImageView] = [:]
    private var renderInProgress = false
    
    private func setupBasedOnLayerToImageView(_ layerToImageView: [PatternLayer: UIImageView]) {
        guard let layers = pattern?.patternLayers else {
            return
        }

        // Add the image views in the same order as the layers.
        var overallBounds: CGRect?
        for layer in layers {
            if let imageView = layerToImageView[layer] {
                addSubview(imageView)
                if let existingBounds = overallBounds {
                    overallBounds = existingBounds.union(imageView.bounds)
                } else {
                    overallBounds = imageView.bounds
                }
            }
        }

        // If we have any views we set the content size and scroll to the bottom.
        if let finalBounds = overallBounds {
            contentSize = finalBounds.size
            scrollRectToVisible(CGRect(x: 0, y: finalBounds.size.height - 1, width: finalBounds.size.width, height: 1), animated: false)
        }
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()

        if patternLayerToImageView.isEmpty && !renderInProgress {
            renderInProgress = true
            renderAllPatternLayers(targetOutputWidth: bounds.size.width) { layerToImageView, error in
                self.renderInProgress = false
                if let layerToImageView = layerToImageView {
                    self.patternLayerToImageView = layerToImageView
                    self.setupBasedOnLayerToImageView(layerToImageView)
                }
            }
        }
    }

}
